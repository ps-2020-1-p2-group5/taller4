pscopy: taller4.o
	gcc -Wall taller4.o -o pscopy

taller4.o: taller4.c
	gcc -Wall -c taller4.c -o taller4.o

clean: 
	rm copia.jpg taller4.o pscopy
