#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
//buffer de 10kb

int main(int argc, char **argv){
    unsigned long *buffer = calloc(0, sizeof(unsigned long));
    char *ruta_fuente = argv[1];
    char *ruta_dest = argv[2];
    int fd_fuente = open(ruta_fuente, O_RDONLY);
    if(fd_fuente<0){
        printf("errno = %d\n", errno);
        perror("Error al abrir el archivo fuente\n");
        return 0;
    }
    umask(0);
    int fd_dest = open(ruta_dest, O_WRONLY|O_CREAT|O_TRUNC, 0666);
    if(fd_dest<0){
        printf("errno = %d\n", errno);
        perror("Error al abrir el archivo destino\n");
        return 0;
    }

    int lectura;
    while(lectura = read(fd_fuente, &buffer, sizeof(buffer)) != 0){
        if(lectura<0){
            printf("errno = %d\n", errno);
            perror("Error al leer el archivo fuente\n");
            return 0;
        }
        write(fd_dest, &buffer, sizeof(buffer));        
    }
    printf("Bytes copiados: %u\n", sizeof(buffer));

    free(buffer);    
    close(fd_fuente);
    close(fd_dest);
    return 0;
}